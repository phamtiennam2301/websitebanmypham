namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblEnvoiceDetail")]
    public partial class tblEnvoiceDetail
    {
        [Key]
        [StringLength(20)]
        public string envoiceDetailId { get; set; }

        [Required]
        [StringLength(20)]
        public string envoiceId { get; set; }

        [Required]
        [StringLength(50)]
        public string Id { get; set; }

        public int amount { get; set; }

        [Column(TypeName = "money")]
        public decimal price { get; set; }

        [Column(TypeName = "date")]
        public DateTime expiryDate { get; set; }

        public virtual tblEnvoice tblEnvoice { get; set; }

        public virtual tblItem tblItem { get; set; }
    }
}
