namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblPromotionType")]
    public partial class tblPromotionType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblPromotionType()
        {
            tblPromotions = new HashSet<tblPromotion>();
        }

        [Key]
        [StringLength(20)]
        public string typeId { get; set; }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        [StringLength(50)]
        public string categoryId { get; set; }

        [StringLength(50)]
        public string Id { get; set; }

        public byte? value { get; set; }

        public virtual tblCategory tblCategory { get; set; }

        public virtual tblItem tblItem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPromotion> tblPromotions { get; set; }
    }
}
