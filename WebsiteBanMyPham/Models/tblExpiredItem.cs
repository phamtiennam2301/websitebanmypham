namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblExpiredItem")]
    public partial class tblExpiredItem
    {
        [Key]
        [StringLength(50)]
        public string expId { get; set; }

        [Required]
        [StringLength(50)]
        public string id { get; set; }

        [Required]
        [StringLength(50)]
        public string itemId { get; set; }

        [Required]
        [StringLength(200)]
        public string name { get; set; }

        [Column(TypeName = "date")]
        public DateTime expiryDate { get; set; }

        public int amount { get; set; }

        [Required]
        [StringLength(50)]
        public string categoryId { get; set; }

        public virtual tblCategory tblCategory { get; set; }

        public virtual tblItem tblItem { get; set; }
    }
}
