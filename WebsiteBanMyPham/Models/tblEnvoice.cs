namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblEnvoice")]
    public partial class tblEnvoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblEnvoice()
        {
            tblEnvoiceDetails = new HashSet<tblEnvoiceDetail>();
        }

        [Key]
        [StringLength(20)]
        public string envoiceId { get; set; }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        [Required]
        [StringLength(20)]
        public string employeeId { get; set; }

        [Required]
        [StringLength(20)]
        public string supplierId { get; set; }

        public DateTime date { get; set; }

        [Column(TypeName = "money")]
        public decimal totalPrice { get; set; }

        public byte status { get; set; }

        public virtual tblEmployee tblEmployee { get; set; }

        public virtual tblSupplier tblSupplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblEnvoiceDetail> tblEnvoiceDetails { get; set; }
    }
}
