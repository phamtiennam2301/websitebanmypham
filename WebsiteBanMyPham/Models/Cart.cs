﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanMyPham.Models
{
  
    public class Cart
    {
        QLBanMyPhamModel db = new QLBanMyPhamModel();
        public string sId { get; set; }
        public string sName { get; set; }
        public string sHinhAnh { get; set; }
        public double dDonGia { get; set; }
        public int iSL { get; set; }
        public double ThanhTien {
            get { return iSL * dDonGia; }
        }

        //Hàm tạo giỏ hàng
        public Cart(string MaSP)
        {
            sId = MaSP;
            tblItem item = db.tblItems.Single(n => n.Id == sId);
            sName = item.name;
            sHinhAnh = item.image;
            dDonGia = double.Parse(item.price.ToString());
            iSL = 1;
    
        }
    }
}