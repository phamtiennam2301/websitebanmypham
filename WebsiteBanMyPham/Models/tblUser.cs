namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblUser")]
    public partial class tblUser
    {
        [Key]
        [StringLength(100)]
        public string username { get; set; }

        [Required]
        [StringLength(100)]
        public string password { get; set; }

        [Required]
        [StringLength(20)]
        public string employeeId { get; set; }

        public byte status { get; set; }

        [Required]
        [StringLength(50)]
        public string roleId { get; set; }

        public virtual tblEmployee tblEmployee { get; set; }

        public virtual tblRole tblRole { get; set; }
    }
}
