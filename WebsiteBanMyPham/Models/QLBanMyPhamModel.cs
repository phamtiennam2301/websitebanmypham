namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class QLBanMyPhamModel : DbContext
    {
        public QLBanMyPhamModel()
            : base("name=QLBanMyPhamModel")
        {
        }

        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tblBill> tblBills { get; set; }
        public virtual DbSet<tblBillDetail> tblBillDetails { get; set; }
        public virtual DbSet<tblCategory> tblCategories { get; set; }
        public virtual DbSet<tblCustomer> tblCustomers { get; set; }
        public virtual DbSet<tblEmployee> tblEmployees { get; set; }
        public virtual DbSet<tblEnvoice> tblEnvoices { get; set; }
        public virtual DbSet<tblEnvoiceDetail> tblEnvoiceDetails { get; set; }
        public virtual DbSet<tblExpiredItem> tblExpiredItems { get; set; }
        public virtual DbSet<tblFeedback> tblFeedbacks { get; set; }
        public virtual DbSet<tblItem> tblItems { get; set; }
        public virtual DbSet<tblOrder> tblOrders { get; set; }
        public virtual DbSet<tblOrderDetail> tblOrderDetails { get; set; }
        public virtual DbSet<tblPayment> tblPayments { get; set; }
        public virtual DbSet<tblPromotion> tblPromotions { get; set; }
        public virtual DbSet<tblPromotionType> tblPromotionTypes { get; set; }
        public virtual DbSet<tblRole> tblRoles { get; set; }
        public virtual DbSet<tblSupplier> tblSuppliers { get; set; }
        public virtual DbSet<tblUser> tblUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblBill>()
                .Property(e => e.billId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBill>()
                .Property(e => e.employeeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBill>()
                .Property(e => e.totalPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblBill>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tblBill>()
                .HasMany(e => e.tblBillDetails)
                .WithRequired(e => e.tblBill)
                .HasForeignKey(e => e.billId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblBill>()
                .HasMany(e => e.tblBillDetails1)
                .WithRequired(e => e.tblBill1)
                .HasForeignKey(e => e.billId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblBillDetail>()
                .Property(e => e.billDetailId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillDetail>()
                .Property(e => e.billId)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillDetail>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblBillDetail>()
                .Property(e => e.price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblCategory>()
                .Property(e => e.categoryId)
                .IsUnicode(false);

            modelBuilder.Entity<tblCategory>()
                .HasMany(e => e.tblExpiredItems)
                .WithRequired(e => e.tblCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<tblCustomer>()
                .HasMany(e => e.tblBills)
                .WithRequired(e => e.tblCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblCustomer>()
                .HasMany(e => e.tblFeedbacks)
                .WithRequired(e => e.tblCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblCustomer>()
                .HasMany(e => e.tblOrders)
                .WithRequired(e => e.tblCustomer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblEmployee>()
                .Property(e => e.employeeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEmployee>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblEmployee>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<tblEmployee>()
                .HasMany(e => e.tblBills)
                .WithRequired(e => e.tblEmployee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblEmployee>()
                .HasMany(e => e.tblEnvoices)
                .WithRequired(e => e.tblEmployee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblEmployee>()
                .HasMany(e => e.tblUsers)
                .WithRequired(e => e.tblEmployee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblEnvoice>()
                .Property(e => e.envoiceId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoice>()
                .Property(e => e.employeeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoice>()
                .Property(e => e.supplierId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoice>()
                .Property(e => e.totalPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblEnvoice>()
                .HasMany(e => e.tblEnvoiceDetails)
                .WithRequired(e => e.tblEnvoice)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblEnvoiceDetail>()
                .Property(e => e.envoiceDetailId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoiceDetail>()
                .Property(e => e.envoiceId)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoiceDetail>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblEnvoiceDetail>()
                .Property(e => e.price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblExpiredItem>()
                .Property(e => e.expId)
                .IsUnicode(false);

            modelBuilder.Entity<tblExpiredItem>()
                .Property(e => e.id)
                .IsUnicode(false);

            modelBuilder.Entity<tblExpiredItem>()
                .Property(e => e.itemId)
                .IsUnicode(false);

            modelBuilder.Entity<tblExpiredItem>()
                .Property(e => e.categoryId)
                .IsUnicode(false);

            modelBuilder.Entity<tblFeedback>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tblFeedback>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblItem>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblItem>()
                .Property(e => e.itemId)
                .IsUnicode(false);

            modelBuilder.Entity<tblItem>()
                .Property(e => e.image)
                .IsUnicode(false);

            modelBuilder.Entity<tblItem>()
                .Property(e => e.categoryId)
                .IsUnicode(false);

            modelBuilder.Entity<tblItem>()
                .HasMany(e => e.tblBillDetails)
                .WithRequired(e => e.tblItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblItem>()
                .HasMany(e => e.tblEnvoiceDetails)
                .WithRequired(e => e.tblItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblItem>()
                .HasMany(e => e.tblExpiredItems)
                .WithRequired(e => e.tblItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblItem>()
                .HasMany(e => e.tblFeedbacks)
                .WithRequired(e => e.tblItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblItem>()
                .HasMany(e => e.tblOrderDetails)
                .WithRequired(e => e.tblItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.employeeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.totalPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.paymentId)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.promotionId)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrder>()
                .Property(e => e.afterTotalPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblBills)
                .WithRequired(e => e.tblOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrder>()
                .HasMany(e => e.tblOrderDetails)
                .WithRequired(e => e.tblOrder)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblOrderDetail>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblOrderDetail>()
                .Property(e => e.price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tblPayment>()
                .Property(e => e.paymentId)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotion>()
                .Property(e => e.promotionId)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotion>()
                .Property(e => e.typeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotionType>()
                .Property(e => e.typeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotionType>()
                .Property(e => e.categoryId)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotionType>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tblPromotionType>()
                .HasMany(e => e.tblPromotions)
                .WithRequired(e => e.tblPromotionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblRole>()
                .Property(e => e.roleId)
                .IsUnicode(false);

            modelBuilder.Entity<tblRole>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<tblRole>()
                .Property(e => e.permissions)
                .IsUnicode(false);

            modelBuilder.Entity<tblRole>()
                .HasMany(e => e.tblUsers)
                .WithRequired(e => e.tblRole)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblSupplier>()
                .Property(e => e.supplierId)
                .IsUnicode(false);

            modelBuilder.Entity<tblSupplier>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<tblSupplier>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<tblSupplier>()
                .HasMany(e => e.tblEnvoices)
                .WithRequired(e => e.tblSupplier)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tblUser>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<tblUser>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<tblUser>()
                .Property(e => e.employeeId)
                .IsUnicode(false);

            modelBuilder.Entity<tblUser>()
                .Property(e => e.roleId)
                .IsUnicode(false);
        }
    }
}
