namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblPromotion")]
    public partial class tblPromotion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblPromotion()
        {
            tblOrders = new HashSet<tblOrder>();
        }

        [Key]
        [StringLength(20)]
        public string promotionId { get; set; }

        [Required]
        [StringLength(200)]
        public string name { get; set; }

        public DateTime startTime { get; set; }

        public DateTime endTime { get; set; }

        [Required]
        [StringLength(20)]
        public string typeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrder> tblOrders { get; set; }

        public virtual tblPromotionType tblPromotionType { get; set; }
    }
}
