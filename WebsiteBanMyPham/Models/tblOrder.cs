namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrder")]
    public partial class tblOrder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblOrder()
        {
            tblBills = new HashSet<tblBill>();
            tblOrderDetails = new HashSet<tblOrderDetail>();
        }

        [Key]
        public int orderId { get; set; }

        [Required]
        [StringLength(100)]
        public string username { get; set; }

        [StringLength(20)]
        public string employeeId { get; set; }

        [Column(TypeName = "money")]
        public decimal totalPrice { get; set; }

        public DateTime date { get; set; }

        [StringLength(20)]
        public string paymentId { get; set; }

        public byte status { get; set; }

        [StringLength(20)]
        public string promotionId { get; set; }

        [Column(TypeName = "money")]
        public decimal afterTotalPrice { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBill> tblBills { get; set; }

        public virtual tblCustomer tblCustomer { get; set; }

        public virtual tblEmployee tblEmployee { get; set; }

        public virtual tblPayment tblPayment { get; set; }

        public virtual tblPromotion tblPromotion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderDetail> tblOrderDetails { get; set; }
    }
}
