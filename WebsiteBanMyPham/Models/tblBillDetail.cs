namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblBillDetail")]
    public partial class tblBillDetail
    {
        [Key]
        [StringLength(20)]
        public string billDetailId { get; set; }

        [Required]
        [StringLength(20)]
        public string billId { get; set; }

        [Required]
        [StringLength(50)]
        public string Id { get; set; }

        public int amount { get; set; }

        [Column(TypeName = "money")]
        public decimal price { get; set; }

        public virtual tblBill tblBill { get; set; }

        public virtual tblBill tblBill1 { get; set; }

        public virtual tblItem tblItem { get; set; }
    }
}
