namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblFeedback")]
    public partial class tblFeedback
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int feedbackId { get; set; }

        [Required]
        [StringLength(100)]
        public string username { get; set; }

        [Required]
        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(400)]
        public string content { get; set; }

        [Column(TypeName = "date")]
        public DateTime date { get; set; }

        public virtual tblCustomer tblCustomer { get; set; }

        public virtual tblItem tblItem { get; set; }
    }
}
