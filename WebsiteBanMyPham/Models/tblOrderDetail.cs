namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblOrderDetail")]
    public partial class tblOrderDetail
    {
        [Key]
        public int orderDetailId { get; set; }

        public int orderId { get; set; }

        [Required]
        [StringLength(50)]
        public string Id { get; set; }

        public int amount { get; set; }

        [Column(TypeName = "money")]
        public decimal price { get; set; }

        public virtual tblItem tblItem { get; set; }

        public virtual tblOrder tblOrder { get; set; }
    }
}
