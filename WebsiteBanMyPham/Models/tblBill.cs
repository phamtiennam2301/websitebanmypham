namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblBill")]
    public partial class tblBill
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblBill()
        {
            tblBillDetails = new HashSet<tblBillDetail>();
            tblBillDetails1 = new HashSet<tblBillDetail>();
        }

        [Key]
        [StringLength(20)]
        public string billId { get; set; }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        public int orderId { get; set; }

        [Required]
        [StringLength(20)]
        public string employeeId { get; set; }

        public byte status { get; set; }

        public DateTime date { get; set; }

        [Column(TypeName = "money")]
        public decimal totalPrice { get; set; }

        [Required]
        [StringLength(100)]
        public string username { get; set; }

        public virtual tblCustomer tblCustomer { get; set; }

        public virtual tblEmployee tblEmployee { get; set; }

        public virtual tblOrder tblOrder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBillDetail> tblBillDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBillDetail> tblBillDetails1 { get; set; }
    }
}
