namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblItem")]
    public partial class tblItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblItem()
        {
            tblBillDetails = new HashSet<tblBillDetail>();
            tblEnvoiceDetails = new HashSet<tblEnvoiceDetail>();
            tblExpiredItems = new HashSet<tblExpiredItem>();
            tblFeedbacks = new HashSet<tblFeedback>();
            tblOrderDetails = new HashSet<tblOrderDetail>();
            tblPromotionTypes = new HashSet<tblPromotionType>();
        }

        [StringLength(50)]
        public string Id { get; set; }

        [StringLength(50)]
        public string itemId { get; set; }

        [StringLength(200)]
        public string name { get; set; }

        [Column(TypeName = "date")]
        public DateTime? expiryDate { get; set; }

        public int? price { get; set; }

        [StringLength(300)]
        public string image { get; set; }

        [Column(TypeName = "ntext")]
        public string description { get; set; }

        [StringLength(50)]
        public string categoryId { get; set; }

        public byte? status { get; set; }

        public int? amount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBillDetail> tblBillDetails { get; set; }

        public virtual tblCategory tblCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblEnvoiceDetail> tblEnvoiceDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblExpiredItem> tblExpiredItems { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFeedback> tblFeedbacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrderDetail> tblOrderDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPromotionType> tblPromotionTypes { get; set; }
    }
}
