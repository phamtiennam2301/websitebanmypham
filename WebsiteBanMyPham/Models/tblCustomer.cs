﻿namespace WebsiteBanMyPham.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;
    using WebsiteBanMyPham.Controllers;

    [Table("tblCustomer")]
    public partial class tblCustomer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblCustomer()
        {
            tblBills = new HashSet<tblBill>();
            tblFeedbacks = new HashSet<tblFeedback>();
            tblOrders = new HashSet<tblOrder>();
        }

        [Key]
        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "{0} không được trống!")]
        [StringLength(100,ErrorMessage = "{0} không nhập quá 100 ký tự!")]
        [UniqueUser(ErrorMessage = "Tên đăng nhập đã tồn tại")]
        public string username { get; set; }

        [Display(Name = "Mật khẩu")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu ít nhất 6 ký tự.")]
        [Required(ErrorMessage = "{0} không được trống!")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [StringLength(250, ErrorMessage = "{0} không nhập quá 250 ký tự!")]
        [Display(Name = "Họ tên")]
        [Required(ErrorMessage = "{0} không được trống!")]
        public string name { get; set; }

        [StringLength(15, ErrorMessage = "{0} không nhập quá 15 ký tự!")]
        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "{0} không được trống!")]
        [Phone(ErrorMessage = "{0} không đúng định dạng!")]
        public string phone { get; set; }

        [StringLength(300, ErrorMessage = "{0} không nhập quá 300 ký tự!")]
        [Display(Name = "Địa chỉ")]
        [Required(ErrorMessage = "{0} không được trống!")]
        public string address { get; set; }

        [StringLength(200, ErrorMessage = "{0} không nhập quá 200 ký tự!")]
        [Display(Name = "Email")]
        [Required(ErrorMessage = "{0} không được trống!")]
        [EmailAddress (ErrorMessage ="{0} không đúng định dạng!")]
        public string email { get; set; }

        public byte status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblBill> tblBills { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFeedback> tblFeedbacks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblOrder> tblOrders { get; set; }
    }
}
