﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteBanMyPham.Common.Repository;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Repositories
{
    public class CategoryRepository : Repository<tblCategory>, ICategoryRepository
    {
        public CategoryRepository(QLBanMyPhamModel dbContext) : base(dbContext)
        {
        }
    }
}