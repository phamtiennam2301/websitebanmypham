﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteBanMyPham.Common.Repository;
using WebsiteBanMyPham.Common.Service;
using WebsiteBanMyPham.Models;
using WebsiteBanMyPham.Repositories;

namespace WebsiteBanMyPham.Services
{
    public class CategoryService : Service<tblCategory>, ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository repository) : base(repository)
        {
            _categoryRepository = repository;
        }
    }
}