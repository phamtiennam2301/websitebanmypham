﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteBanMyPham.Common.Service;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Services
{
    public interface ICategoryService : IService<tblCategory>
    {
    }
}