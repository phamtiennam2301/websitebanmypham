﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        QLBanMyPhamModel db = new QLBanMyPhamModel();
        public ActionResult Index()
        {
            return View(db.tblItems.Where(n => n.status == 2).Take(2).ToList());
        }
        public PartialViewResult Category()
        {
            var lstCate = db.tblCategories.ToList();
            return PartialView(lstCate);
        }
        public ViewResult ItemCategory (string Id)
        {
            tblCategory category = db.tblCategories.SingleOrDefault(n => n.categoryId == Id);
            if(category==null)
                {
                Response.StatusCode = 404;
                return null;
                }
            List<tblItem> lstItem = db.tblItems.Where(n => n.categoryId == Id).Where(n=>n.status==2).ToList();
            if(lstItem.Count == 0)
            {
                ViewBag.Home = "Không có sản phẩm nào thuộc danh mục này!";
            }
            return View(lstItem);
        }
    }
}