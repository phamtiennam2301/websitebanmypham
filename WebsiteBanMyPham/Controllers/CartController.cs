﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Controllers
{
    public class CartController : Controller
    {
        QLBanMyPhamModel db = new QLBanMyPhamModel();
        // lấy giỏ hàng
        public List<Cart> LayGioHang()
        {
            List<Cart> lstGioHang = Session["Cart"] as List<Cart>;
            if(lstGioHang== null)
            {
                //Nếu cart chưa tồn tại thì tiến hành tạo list cart
                lstGioHang = new List<Cart>();
                Session["Cart"] = lstGioHang;
            }
            return lstGioHang;
        }
        //thêm giỏ hàng
        public ActionResult ThemGioHang(string sId, string sUrl)
        {
            tblItem item = db.tblItems.SingleOrDefault(n => n.Id == sId);
            if (item == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //lấy session giỏ hàng
            List<Cart> lstGioHang = LayGioHang();
            //kiểm tra id SP đã tồn tại trong session chưa?
            Cart cart = lstGioHang.Find(n => n.sId == sId);
            if (cart == null)
            {
                cart = new Cart(sId);
                lstGioHang.Add(cart);
                return Redirect(sUrl);
            }
            else
            {
                cart.iSL++;
                return Redirect(sUrl);
            }
        }
        //cập nhật giỏ hàng
        public ActionResult CapNhatGioHang(string sId, FormCollection f)
        {
            tblItem item = db.tblItems.SingleOrDefault(n => n.Id == sId);
            if (item == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<Cart> lstGioHang = LayGioHang();
            Cart cart = lstGioHang.SingleOrDefault(n => n.sId == sId);
            if (cart != null)
            {
                cart.iSL = int.Parse(f["txtSL"].ToString());
            }
            return RedirectToAction("Cart");
        }
        //Xóa giỏ hàng
        public ActionResult XoaGioHang(string sId)
        {
            tblItem item = db.tblItems.SingleOrDefault(n => n.Id == sId);
            if (item == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            List<Cart> lstGioHang = LayGioHang();
            Cart cart = lstGioHang.SingleOrDefault(n => n.sId == sId);
            if (cart != null)
            {
                lstGioHang.RemoveAll(n => n.sId == sId);
            }
            if (lstGioHang.Count == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Cart");
        }
        //xây dựng page giỏ hàng
        public ActionResult Cart()
        {
            if (Session["Cart"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            List<Cart> lstGioHang = LayGioHang();
            return View(lstGioHang);
        }
        //Tính tổng SL & tổng tiền
        //Tổng SL
        private int TongSL()
        {
            int iTongSL = 0;
            List<Cart> lstGioHang = Session["Cart"] as List<Cart>;
            if (lstGioHang != null)
            {
                iTongSL = lstGioHang.Sum(n => n.iSL);
            }
            return iTongSL; 
        }
        //Thành tiền
        private double TongTien()
        {
            double dTongTien = 0;
            List<Cart> lstGioHang = Session["Cart"] as List<Cart>;
            if (lstGioHang != null)
            {
                dTongTien = lstGioHang.Sum(n => n.ThanhTien);
            }
            return dTongTien;
        }
        //tạo partial giỏ hàng
        public ActionResult CartPartial()
        {
            if (TongSL() == 0)
            {
                return PartialView();
            }
            ViewBag.TongSL = TongSL();
            ViewBag.TongTien = TongTien();
            return PartialView();
        }
        //tạo view chỉnh sửa giỏ hàng
        public ActionResult UpdateCart()
        {
            if (Session["Cart"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            List<Cart> lstGioHang = LayGioHang();
            return View(lstGioHang);
        }
        ////Đặt hàng
        //Xây dựng chức năng đặt hàng
        [HttpPost]
        public ActionResult DatHang()
        {
            //Kiểm tra đăng nhập
            if (Session["TaiKhoan"] == null || Session["TaiKhoan"].ToString() == "")
            {
                return RedirectToAction("DangNhap", "KhachHang");
            }
            //Tạo đơn đặt hàng
            if(Session["Cart"] == null)
            {
                RedirectToAction("Index", "Home");
            }
            tblOrder order = new tblOrder();
            tblCustomer cust = (tblCustomer)Session["TaiKhoan"];
            List<Cart> carts = LayGioHang();
            order.username = cust.username;
            order.date = DateTime.Now;
            order.totalPrice =(decimal)carts.Sum(n => n.ThanhTien);
            order.afterTotalPrice = (decimal)carts.Sum(n => n.ThanhTien);
            db.tblOrders.Add(order);
            db.SaveChanges();

            // thêm chi tiết đơn hàng
            foreach(var item in carts)
            {
                tblOrderDetail detail = new tblOrderDetail();
                detail.orderId = order.orderId;
                detail.Id = item.sId;
                detail.amount = item.iSL;
                detail.price = (decimal)item.dDonGia;
                db.tblOrderDetails.Add(detail);
            }
            db.SaveChanges();
            return RedirectToAction("Index","Home");
        }
    }
}