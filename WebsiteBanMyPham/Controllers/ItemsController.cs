﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Controllers
{
    public class ItemsController : Controller
    {
        // GET: Items
        QLBanMyPhamModel db = new QLBanMyPhamModel();
        public ActionResult Index()
        {
            return View(db.tblItems.ToList());
        }
        public ViewResult ItemDetail(string Id)
        {
            tblItem items = db.tblItems.SingleOrDefault(n => n.Id == Id);
            if (items == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            ViewBag.name = db.tblCategories.Single(n => n.categoryId == items.categoryId).name;
            return View(items);
        }
    }
}