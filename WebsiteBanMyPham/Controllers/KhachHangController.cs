﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Controllers
{
    public class KhachHangController : Controller
    {
        // GET: KhachHang
        QLBanMyPhamModel db = new QLBanMyPhamModel();
        public ActionResult Index()
        {
            return View();
        }
        //Action đăng ký
        [HttpGet]
        public ActionResult DangKy()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DangKy(tblCustomer cust )
        {
            if (ModelState.IsValid)
            {
                //Insert data vào CSDL
                db.tblCustomers.Add(cust);
                //Lưu DB
                db.SaveChanges();
                ViewBag.ThongBao = "Đăng ký thành công!";
            }
            return View();
        }
        //Action Login
        [HttpGet]
        public ActionResult DangNhap()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DangNhap(FormCollection f)
        {
            string sUsr = f["txtUsername"].ToString();
            string sMK =  f["txtMK"].ToString();
            tblCustomer customer = db.tblCustomers.SingleOrDefault(n => n.username == sUsr && n.password == sMK);
            if(customer != null)
            {
                Session["TaiKhoan"] = customer;

                return RedirectToAction("Index", "Home");
            }
            ViewBag.ThongBao = "Đăng nhập không thành công.";
            return View();
        }
    }
    public class UniqueUserAttribute : ValidationAttribute
    {
        public override bool IsValid(object user)
        {
            QLBanMyPhamModel db = new QLBanMyPhamModel();
            var validateName = db.tblCustomers.SingleOrDefault(
                n => n.username == (string)user);
            return validateName == null;
        }
    }
}