﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebsiteBanMyPham.Common.Service;
using WebsiteBanMyPham.Models;
using WebsiteBanMyPham.Services;

namespace WebsiteBanMyPham.Areas.Admin.Controllers
{
    public class ManagerCategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        
        public ManagerCategoriesController(ICategoryService service)
        {
            _categoryService = service;
        }

        // GET: Admin/ManagerCategories
        public ActionResult Index()
        {
            var listCategories = _categoryService.GetAll();
            return View(listCategories);
        }
    }
}