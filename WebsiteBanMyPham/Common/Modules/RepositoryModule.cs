﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebsiteBanMyPham.Common.Modules
{
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("WebsiteBanMyPham"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.Load("WebsiteBanMyPham"))
                .Where(t => t.Name.EndsWith("Repositories"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}