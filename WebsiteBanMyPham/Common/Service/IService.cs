﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanMyPham.Common.Service
{
    public interface IService<TEntity>
        where TEntity : class
    {
        IEnumerable<TEntity> GetAll();

        TEntity GetById(object id);

        void Create(TEntity entity);

        void Delete(TEntity entity);

        void Delete(object id);

        void Update(TEntity entity, object id);

        void Dispose();
    }
}