﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebsiteBanMyPham.Common.Repository;

namespace WebsiteBanMyPham.Common.Service
{
    public class Service<TEntity> : IService<TEntity>
        where TEntity : class
    {
        private const string ParamNull = "Entity input cant null";

        private readonly IRepository<TEntity> _repository;

        public Service(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public void Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(ParamNull);
            }
            _repository.Insert(entity);
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(ParamNull);
            }
            _repository.Delete(entity);
        }

        public void Delete(object id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(ParamNull);
            }
            _repository.Delete(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public TEntity GetById(object id)
        {
            return _repository.GetByID(id);
        }

        public void Update(TEntity entity, object id)
        {
            _repository.Update(entity, id);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}