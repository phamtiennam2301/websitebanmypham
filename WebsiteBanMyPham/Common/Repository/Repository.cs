﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebsiteBanMyPham.Models;

namespace WebsiteBanMyPham.Common.Repository
{
    public class Repository<TEntity>: IRepository<TEntity>
        where TEntity : class
    {
        protected readonly QLBanMyPhamModel _dbContext;

        private readonly DbSet<TEntity> _dbSet;

        public Repository(QLBanMyPhamModel dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public ICollection<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity GetByID(object Id)
        {
            return _dbSet.Find(Id);
        }

        public void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(object id)
        {
            TEntity entityDeleted = _dbSet.Find(id);
            Delete(entityDeleted);
        }

        public void Delete(TEntity entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached)
            {
                _dbSet.Attach(entity);
            }
            _dbSet.Remove(entity);
            _dbContext.SaveChanges();
        }

        public void Update(TEntity entity, object id)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            TEntity exist = _dbSet.Find(id);
            if (exist != null)
            {
                _dbContext.Entry(exist).CurrentValues.SetValues(entity);
                _dbContext.SaveChanges();
            }
        }

        public void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}