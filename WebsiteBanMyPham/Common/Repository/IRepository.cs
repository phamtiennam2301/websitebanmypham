﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebsiteBanMyPham.Common.Repository
{
    public interface IRepository<TEntity> where TEntity: class
    {
        ICollection<TEntity> GetAll();

        TEntity GetByID(object Id);

        void Insert(TEntity entity);

        void Delete(object id);

        void Delete(TEntity entity);

        void Update(TEntity entity, object id);

        void SaveChanges();

        void Dispose();
    }
}